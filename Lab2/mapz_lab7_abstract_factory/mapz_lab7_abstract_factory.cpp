#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>

class AbstractProductA {
public:
	virtual ~AbstractProductA() {};
	virtual std::string UsefulFunctionA() const = 0;
};

class ConcreteProductA1 : public AbstractProductA {
public:
	std::string UsefulFunctionA() const override {
		return "fishing rot 1";
	}
};

class ConcreteProductA2 : public AbstractProductA {
	std::string UsefulFunctionA() const override {
		return "fishing rot 2";
	}
};

class AbstractProductB {

public:
	virtual ~AbstractProductB() {};
	virtual std::string UsefulFunctionB() const = 0;

	virtual std::string AnotherUsefulFunctionB(const AbstractProductA& collaborator) const = 0;
};


class ConcreteProductB1 : public AbstractProductB {
public:
	std::string UsefulFunctionB() const override {
		return "Factory 1 result:";
	}

	std::string AnotherUsefulFunctionB(const AbstractProductA& collaborator) const override {
		const std::string result = collaborator.UsefulFunctionA();
		return "Fisherman 1 used: " + result + "";
	}
};

class ConcreteProductB2 : public AbstractProductB {
public:
	std::string UsefulFunctionB() const override {
		return "Factory 2 result:";
	}

	std::string AnotherUsefulFunctionB(const AbstractProductA& collaborator) const override {
		const std::string result = collaborator.UsefulFunctionA();
		return "Fisherman 2 used: " + result + "";
	}
};

class AbstractFactory {
public:
	virtual AbstractProductA* CreateProductA() const = 0;
	virtual AbstractProductB* CreateProductB() const = 0;
};

class ConcreteFactory1 : public AbstractFactory {
public:
	AbstractProductA* CreateProductA() const override {
		return new ConcreteProductA1();
	}
	AbstractProductB* CreateProductB() const override {
		return new ConcreteProductB1();
	}
};

class ConcreteFactory2 : public AbstractFactory {
public:
	AbstractProductA* CreateProductA() const override {
		return new ConcreteProductA2();
	}
	AbstractProductB* CreateProductB() const override {
		return new ConcreteProductB2();
	}
};

void ClientCode(const AbstractFactory& factory) {
	const AbstractProductA* product_a = factory.CreateProductA();
	const AbstractProductB* product_b = factory.CreateProductB();
	std::cout << product_b->UsefulFunctionB() << "\n";
	std::cout << product_b->AnotherUsefulFunctionB(*product_a) << "\n";
	delete product_a;
	delete product_b;
}

int main()
{
	std::cout << "Factory 1 test:\n";
	ConcreteFactory1* f1 = new ConcreteFactory1();
	ClientCode(*f1);
	delete f1;
	std::cout << std::endl;
	std::cout << "Factory 2 test:\n";
	ConcreteFactory2* f2 = new ConcreteFactory2();
	ClientCode(*f2);
	delete f2;
	return 0;
}
