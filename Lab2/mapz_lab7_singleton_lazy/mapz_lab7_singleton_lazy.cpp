#include <iostream>
#include <chrono>
#include <thread>

class Singleton
{
protected:
	Singleton(const std::string value) : value_(value)
	{
	}

	static Singleton* singleton_;

	std::string value_;

public:

	Singleton(Singleton& other) = delete;
	void operator=(const Singleton&) = delete;

	static Singleton* GetInstance(const std::string& value);
	
	std::string value() const {
		return value_;
	}
};

Singleton* Singleton::singleton_ = nullptr;;

Singleton* Singleton::GetInstance(const std::string& value)
{
	if (singleton_ == nullptr) {
		singleton_ = new Singleton(value);
	}
	return singleton_;
}

void Player1() {
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	Singleton* singleton = Singleton::GetInstance("Fisherman A");
	std::cout << singleton->value() << "\n";
}

void Player2() {
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	Singleton* singleton = Singleton::GetInstance("Fisherman B");
	std::cout << singleton->value() << "\n";
}


int main()
{
	std::cout << "If names are the same - singleton worked\n" <<"Result:\n";
	std::thread t1(Player1);
	std::thread t2(Player2);
	t1.join();
	t2.join();

	return 0;
}
