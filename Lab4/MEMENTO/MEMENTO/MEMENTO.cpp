﻿#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <vector>
#include <ctime>
#include <mutex>

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)

using namespace std;

class ThisOriginator
{
private:
	string statte;

	string stringGenerateS(int length = 10)
	{
		const char alph[] =
			"1234567890";
		int Lengthofstring = sizeof(alph) - 1;

		string string_random;
		for (int i = 0; i < length; i++)
		{
			string_random += alph[std::rand() % Lengthofstring];
		}
		return string_random;
	}

public:
	ThisOriginator(string statte) : statte(statte)
	{
		cout << "Рибак: Поточний рахунок : " << this->statte << "\n";
	}

	void DoSomething()
	{
		cout << "Рибак: ловить рибу. Рахунок очок змiнюється...\n";
		this->statte = this->stringGenerateS(5);
		cout << "Рибак: поточний рахунок змiнився : " << this->statte << "\n";
	}

	MyMemento* Savedata()
	{
		return new ThisMemento(this->statte);
	}

	void Restof(MyMemento* memento)
	{
		this->statte = memento->statte();
		cout << "Рибак: поточний рахунок змiнився : " << this->statte << "\n";
	}
};


class MyMemento
{
public:
	virtual string GetMyName() const = 0;
	virtual string getthisdate() const = 0;
	virtual string statte() const = 0;
	virtual string time() const = 0;
	virtual string data() const = 0;
};

class ThisMemento : public MyMemento
{
private:
	string statte;
	string datte;

public:
	ThisMemento(string statte) : statte(statte)
	{
		this->statte = statte;
		time_t now = time(0);
		this->datte = ctime(&now);
	}
	string statte() const override
	{
		return this->statte;
	}

	string GetMyName() const override
	{
		return this->datte + " / (" + this->statte.substr(0, 9) + "...)";
	}
	string getthisdate() const override
	{
		return this->datte;
	}
};

class ThisOriginator
{
private:
	string statte;

	string stringGenerateS(int length = 10)
	{
		const char alphof[] =
			"1234567890";
		int stringLength = sizeof(alphof) - 1;

		string stringrandom;
		for (int i = 0; i < length; i++)
		{
			stringrandom += alphof[std::rand() % stringLength];
		}
		return stringrandom;
	}

public:
	ThisOriginator(string statte) : statte(statte)
	{
		cout << "Рибак: Поточний рахунок : " << this->statte << "\n";
	}

	void DoSomething()
	{
		cout << "Рибак: ловить рибу. Рахунок очок змiнюється...\n";
		this->statte = this->stringGenerateS(5);
		cout << "Рибак: поточний рахунок змiнився : " << this->statte << "\n";
	}

	MyMemento* Savedata()
	{
		return new ThisMemento(this->statte);
	}

	void Restof(MyMemento* memento)
	{
		this->statte = memento->statte();
		cout << "Рибак: поточний рахунок змiнився : " << this->statte << "\n";
	}
};


class MyCaretaker
{

private:
	vector<MyMemento*> mymymementos;
	ThisOriginator* originrrrr;

public:
	MyCaretaker(ThisOriginator* originator) : originrrrr(originator)
	{
		this->originrrrr = originator;
	}

	void Backupdata()
	{
		cout << "\nСервер: зберiгає поточоний стан...\n";
		this->mymymementos.push_back(this->originrrrr->Savedata());
	}
	void Undin()
	{
		if (!this->mymymementos.size())
		{
			return;
		}
		MyMemento* memento = this->mymymementos.back();
		this->mymymementos.pop_back();
		cout << "Сервер: вiдновлює попереднiй стан: " << memento->GetMyName() << "\n";
		try
		{
			this->originrrrr->Restof(memento);
		}
		catch (...)
		{
			this->Undin();
		}
	}
	void ShowMyHistoryof() const
	{
		cout << "Сервер: показати список збережених знiмкiв рахункiв:\n";
		for (MyMemento* memento : this->mymymementos)
		{
			cout << memento->GetMyName() << "\n";
		}
	}
};

void codeofClient()
{
	ThisOriginator* thismyoriginator = new ThisOriginator("00000.");
	MyCaretaker* thismycaretaker = new MyCaretaker(thismyoriginator);
	thismycaretaker->Backupdata();
	thismyoriginator->DoSomething();
	thismycaretaker->Backupdata();
	thismyoriginator->DoSomething();
	thismycaretaker->Backupdata();
	thismyoriginator->DoSomething();
	cout << "\n";
	thismycaretaker->ShowMyHistoryof();
	cout << "\nКлiєнт: вiдновлюю попереднiй стан\n\n";
	thismycaretaker->Undin();
	cout << "\nКлiєнт: вiдновлюю попереднiй стан\n\n";
	thismycaretaker->Undin();

	delete thismyoriginator;
	delete thismycaretaker;
}

int main()
{
	setlocale(LC_CTYPE, "ukr");
	std::srand(static_cast<unsigned int>(std::time(NULL)));
	codeofClient();
	return 0;
}
