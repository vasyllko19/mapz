﻿#include <iostream>
#include <map>
#include <mutex>
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>
#include <string>
#include <chrono>
#include <thread>

using namespace std;
class MyContext;


void Codeofclient()
{
	MyContext* context = new MyContext(new thisFirstState);
	context->FirstRequest();
	context->SecondRequest();
	delete context;
}
class thissecondState : public MyState
{
public:
	void firstHandle() override
	{
		cout << "thissecondState опрацьовує запит 1.\n";
	}
	void SecondHandle() override
	{
		cout << "thissecondState опрацьовує запит 2.\n";
		cout << "thissecondState change сstate.\n";
		this->thiscontext_->To_Transition(new thisFirstState);
	}
};
class MyState
{
protected:
	MyContext* thiscontext_;

public:
	virtual ~MyState()
	{
	}

	void contextSet____(MyContext* contextttt)
	{
		this->thiscontext_ = contextttt;
	}

	virtual void firstHandle() = 0;
	virtual void SecondHandle() = 0;
};


class MyContext
{
private:
	MyState* state_;

public:
	MyContext(MyState* state) : state_(nullptr)
	{
		this->To_Transition(state);
	}
	~MyContext()
	{
		delete state_;
	}

	void To_Transition(MyState* state)
	{
		std::cout << "Сесiя гри: перехiд стану до " << typeid(*state).name() << ".\n";
		if (this->state_ != nullptr)
			delete this->state_;
		this->state_ = state;
		this->state_->contextSet____(this);
	}

	void FirstRequest()
	{
		this->state_->firstHandle();
	}
	void SecondRequest()
	{
		this->state_->SecondHandle();
	}
};


class thisFirstState : public MyState
{
public:
	void firstHandle() override;

	void SecondHandle() override
	{
		cout << "ConcreteStateA опрацьовує запит 2.\n";
	}
};



void thisFirstState::firstHandle()
{
	{
		cout << "ConcreteStateA опрацьовує запит 1.\n";
		cout << "ConcreteStateA змiнює стан.\n";

		this->thiscontext_->To_Transition(new thissecondState);
	}
}



int main()
{
	setlocale(LC_CTYPE, "ukr");
	Codeofclient();
	return 0;
}

