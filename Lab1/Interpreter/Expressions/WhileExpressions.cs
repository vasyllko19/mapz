﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class WhileExpressions : IExpression
    {
        public IExpression Condittion { get; private set; }
        public IExpression TrueExpressions { get; private set; }
		//----------------------------------------------------------------------
		public WhileExpressions(IExpression condition, IExpression trueExpression)
        {
            Condittion = condition;
            TrueExpressions = trueExpression;
        }
        //----------------------------------------------------------------------
        public object Execute(Contextt context)
        {
            //execute the context
            while ((bool)Condittion.Execute(context))
            {
                TrueExpressions.Execute(context);
            }
            return null;
        }
    }
    //--------------------------------------------------------------------------
    class WhileExpressionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking if the token is "while"
            if (tokens[position].getType() != TokenType.While)
                return null;

            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"While statement syntax error: '(' is missing!");

            position++;
            var condition = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"While statement syntax error: ')' is missing!");
            //adding all the tokens to the tree
            position++;
            var tokensList = new List<Token>();
            while (tokens[position].getType() != TokenType.End)
            {
                tokensList.Add(tokens[position]);
                position++;
            }
            var trueExpression = new SyntaxxAnalyzer().TreeBuilderr(tokensList);
            //checking if there is "end"
            if (tokens[position].getType() != TokenType.End)
                throw new Exception($"While statement syntax error: keyword 'end' is missing!");
            else
            {
                position++;
                return new WhileExpressions(condition, trueExpression);
            }
        }

        public WhileExpressionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
    }
}
//------------------------------------------------------------------------------