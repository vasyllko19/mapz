﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //class to vork work with variables
    //--------------------------------------------------------------------------
    class VarriableExpressions : IExpression
    {
        public string VariabbleName { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            if (context.Variables.ContainsKey(VariabbleName))
            {
                Debug.WriteLine(context.Variables[VariabbleName]);
                return context.Variables[VariabbleName];
            }
            else throw new Exception($"Variable {VariabbleName} do not exist!");
        }
        //----------------------------------------------------------------------
        public VarriableExpressions(string variable)
        {
            VariabbleName = variable;
        }
    }
    //--------------------------------------------------------------------------
    class VariableExpressionCreatorr : IExpressionCreator
    {
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.VariableName)
                return null;
            else
            {
                return new VarriableExpressions(tokens[position++].GetLexem());
            }
        }
    }
}
//------------------------------------------------------------------------------