﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{

    class BoolDeclCreator : IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.Bool)
                return null;
            else
            {
                var variable = tokens[++position];
                if (variable.getType() == TokenType.VariableName)
                {
                    position++;
                    return new BoolDeclExpression(variable.GetLexem());
                }
                else throw new Exception("Bool Declaration Error!");
            }
        }
    }
    //--------------------------------------------------------------------------
    class BoolDeclExpression : IExpression
    {
        public object Execute(Contextt context)
        {
            context.Variables.Add(InputData, false);
            return null;
        }
        public string InputData { get; private set; }

		public BoolDeclExpression(string inputData)
        {
            InputData = inputData;
        }
       
    }
    //--------------------------------------------------------------------------
    
}
//------------------------------------------------------------------------------