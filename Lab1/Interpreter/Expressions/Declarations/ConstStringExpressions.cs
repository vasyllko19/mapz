﻿//------------------------------------------------------------------------------
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    class ConsttStringExpressions : IExpression
    {

        public object Execute(Contextt context)
        {
            return Value;
        }

        public string Value { get; private set; }
		//----------------------------------------------------------------------
		public ConsttStringExpressions(string value)
        {
            Value = value;
        }
        //----------------------------------------------------------------------
       
    }
    //--------------------------------------------------------------------------
    public class ConsttStringCreator : IExpressionCreator
    {
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            if (tokens[position].getType() != TokenType.StringConst)
                return null;
            else
                return new ConsttStringExpressions(tokens[position++].GetLexem().Trim('"'));
        }
    }
}
//------------------------------------------------------------------------------