﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//------------------------------------------------------------------------------
namespace Interpretator
{

    class MinusExpressionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //------------------------------------------------------------------------------
        public MinusExpressionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //------------------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            var startPosition = position;
            //checking if there are brackets arount the expression
            if (tokens[position].getType() != TokenType.LeftBracket)
                return null;

            position++;
            var leftOperand = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.Minus)
            {
                position = startPosition;
                return null;
            }

            position++;
            var rightOPerand = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() == TokenType.RightBracket)
            {
                position++;
                return new MinusExpressions(leftOperand, rightOPerand);
            }
            else
            {
                position = startPosition;
                throw new Exception("SyntaxErrorException in 'MINUS' declaration!");
            }
        }
    }
    //--------------------------------------------------------------------------
    class MinusExpressions : IExpression
    {
        public IExpression LeftOperandd { get; private set; }
        public IExpression RightOperandd { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            var left = LeftOperandd.Execute(context);
            var right = RightOperandd.Execute(context);
            if (LeftOperandd is ConsttIntExpressions)
            {
                if ((LeftOperandd is ConsttIntExpressions) || ((RightOperandd is VarriableExpressions) && right.GetType() == left.GetType()))
                    return (object)((dynamic)left - (dynamic)right);
            }
            else if (LeftOperandd is VarriableExpressions)
            {
                if ((RightOperandd is ConsttIntExpressions) && right.GetType() == left.GetType() || ((RightOperandd is VarriableExpressions) && right.GetType() != typeof(String)))
                    return (object)((dynamic)left - (dynamic)right);
            }
            throw new Exception($"MinusException: this operation available only for numbers!"); ;
        }

        public MinusExpressions(IExpression left, IExpression right)
        {
            LeftOperandd = left;
            RightOperandd = right;
        }

		public override string ToString()
		{
			return String.Format("{{{0} - {1}}}", LeftOperandd, RightOperandd);
		}
	}
    //------------------------------------------------------------------------------
   
}
//------------------------------------------------------------------------------