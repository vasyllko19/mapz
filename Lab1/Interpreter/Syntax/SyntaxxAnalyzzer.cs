﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //contains all the variables
    //--------------------------------------------------------------------------
    public class Contextt
    {
        public Dictionary<string, object> Variables = new Dictionary<string, object>();
    }
    //--------------------------------------------------------------------------
    //executes given expression
    //--------------------------------------------------------------------------
    public interface IExpression
    {
        object Execute(Contextt context);
    }
    //--------------------------------------------------------------------------
    //creates expressions
    //--------------------------------------------------------------------------
    public interface IExpressionCreator
    {
        IExpression CreateExpressions(ref List<Token> tokens, ref int position);
    }
    //--------------------------------------------------------------------------
    //syntaxys analyzer
    //--------------------------------------------------------------------------
    public class SyntaxxAnalyzer
    {
        public static string AnalyzerOutput = "";           //out to console
        public int Position = 0;                            //current position
        private List<Token> _tokens = null;                 //given list of tokens

        public ReadOnlyCollection<Token> InputTokens => new ReadOnlyCollection<Token>(_tokens); 

        private List<IExpressionCreator> _expressionPattern;//list of expressions
        //----------------------------------------------------------------------
        public SyntaxxAnalyzer()
        {
            _expressionPattern = new List<IExpressionCreator>
            {
                new AssignmenCreatorr(this),
                new ConsttIntCreator(),
                new ConsttStringCreator(),
                new TrueFalseExpressionsCreatorr(),
                new PlusExpressionsCreatorr(this),
                new MinusExpressionsCreatorr(this),
                new ComparisonCreatorr(this),
                new VariableExpressionCreatorr(),
                new PrintFunctionsCreatorr(this),
                new IntDeclCreator(),
                new BoolDeclCreator(),
                new IfElseExpressionsCreatorr(this),
                new WhileExpressionsCreatorr(this),
                new DoWhileExpressionsCreatorr(this),
                new StringDeclCreator(),
                new VisittFunctionsCreatorr(this),
                new TagExpressionsCreatorr(this),
                new CountFunctionsCreatorr(this),
                new ToFileCreatorr(this),
                new IntToStringgCreatorr(this)
            };
        }
        //----------------------------------------------------------------------
        //builds the tree and reseives
        //----------------------------------------------------------------------
        public IExpression TreeBuilderr(List<Token> tokens)
        {
            int position = 0;
            IList<IExpression> expressions = new List<IExpression>();//built tree
            while (position < tokens.Count)
            {
                var current = GetCurrentExpressions(tokens, ref position);
                expressions.Add(current);
            }
            return new SequanceExpressions(expressions);
        }
        //----------------------------------------------------------------------
        //checking if the pattern exists
        //----------------------------------------------------------------------
        public IExpression GetCurrentExpressions(List<Token> tokens, ref int position)
        {
            foreach (var expressionCreator in _expressionPattern)
            {
                var result = expressionCreator.CreateExpressions(ref tokens, ref position);
                if (result != null)
                    return result;
            }
            throw new Exception($"No such pattern starting at {tokens[position]}");
        }
        //----------------------------------------------------------------------
        //sequense of executing of expressions
        //----------------------------------------------------------------------
        public class SequanceExpressions : IExpression
        {
            private readonly List<IExpression> _expressionList;
			public readonly IReadOnlyList<IExpression> ExpressionList;
            //------------------------------------------------------------------
            public SequanceExpressions(IList<IExpression> expressions)
            {
                _expressionList = new List<IExpression>(expressions);
				ExpressionList = _expressionList;
            }
            //------------------------------------------------------------------
            public object Execute(Contextt context)
            {
                foreach (var expression in _expressionList)
                {
                    expression.Execute(context);
                }
                return null;
            }
        }
    }
}
//------------------------------------------------------------------------------