﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpretator
{
	public class TreePrinter
	{
		public string PrintTree(IExpression expression)
		{
			StringBuilder builder = new StringBuilder();
			PrintTreeRecursively(ref builder, 0, expression);
			return builder.ToString();
		}

		private void PrintTreeRecursively(ref StringBuilder builder, int offset, IExpression expression)
		{
			switch (expression)
			{
				case SyntaxxAnalyzer.SequanceExpressions seqExpression:
				{
					foreach (IExpression child in seqExpression.ExpressionList)
					{
						PrintTreeRecursively(ref builder, offset, child);
						builder.AppendLine();
					}
				}
				break;

				case PlussExpressions plusExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("PLUS");
					PrintTreeRecursively(ref builder, offset + 1, plusExpression.LeftOperandd);
					PrintTreeRecursively(ref builder, offset + 1, plusExpression.RightOperandd);
				}
				break;

				case MinusExpressions minusExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("MINUS");
					PrintTreeRecursively(ref builder, offset + 1, minusExpression.LeftOperandd);
					PrintTreeRecursively(ref builder, offset + 1, minusExpression.RightOperandd);
				}
				break;

				case AssignmentExpressions assignExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("ASSIGN");
					PrintTreeRecursively(ref builder, offset + 1, new VarriableExpressions(assignExpression.LeftOperandd));
					PrintTreeRecursively(ref builder, offset + 1, assignExpression.RightOperand);
				}
				break;

				case IntDeclExpression intDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-INT");
					PrintTreeRecursively(ref builder, offset + 1, new ConsttStringExpressions(intDeclExpression.InputData));
				}
				break;

				case BoolDeclExpression boolDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-BOOL");
					PrintTreeRecursively(ref builder, offset + 1, new ConsttStringExpressions(boolDeclExpression.InputData));
				}
				break;

				case StringDeclExpression strDeclExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("DECLARE-STRING");
					PrintTreeRecursively(ref builder, offset + 1, new ConsttStringExpressions(strDeclExpression.InputData));
				}
				break;

				case ConsttIntExpressions constIntExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(constIntExpression.Value.ToString());
				}
				break;

				case TrueFalseExpressions constBoolExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(constBoolExpression.Vallue.ToString());
				}
				break;

				case ConsttStringExpressions constStrExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("\"{0}\"", constStrExpression.Value));
				}
				break;

				case IfElseExpressions ifElseExpression:
				{
					AppendSpaces(ref builder, offset);
					if (ifElseExpression.FalseExpressions != null)
					{
						builder.AppendLine(String.Format("IF-ELSE"));
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.Condition);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.TrueExpressions);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.FalseExpressions);
					}
					else
					{
						builder.AppendLine(String.Format("IF"));
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.Condition);
						PrintTreeRecursively(ref builder, offset + 1, ifElseExpression.TrueExpressions);
					}
				}
				break;

				case ComparisonnExpressions comparisionExpression:
				{
					Dictionary<string, string> signToName = new Dictionary<string, string>()
					{
						{ "==", "EQUAL" },
						{ "!=", "NOT-EQUAL" },
						{ ">", "MORE" },
						{ "<", "LESS" },
						{ ">=", "MORE-OR-EQUAL" },
						{ "<=", "LESS-OR-EQUAL" }
					};
					AppendSpaces(ref builder, offset);
					builder.AppendLine(signToName[comparisionExpression.Sign]);
					PrintTreeRecursively(ref builder, offset + 1, comparisionExpression.LeftOperandd);
					PrintTreeRecursively(ref builder, offset + 1, comparisionExpression.RightOperandd);
				}
				break;

				case WhileExpressions whileExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("WHILE-LOOP"));
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.Condittion);
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.TrueExpressions);
				}
				break;

				case DoWhileExpressions whileExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("DO-WHILE-LOOP"));
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.Condittion);
					PrintTreeRecursively(ref builder, offset + 1, whileExpression.TrueExpressions);
				}
				break;

				case VarriableExpressions varExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(varExpression.VariabbleName);
				}
				break;

				case TagExpressions tagExpression:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("TAG"));
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.Url);
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.TagName);
					PrintTreeRecursively(ref builder, offset + 1, tagExpression.TagNum);
				}
				break;

				case PrinttFunction printFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("PRINT"));
					PrintTreeRecursively(ref builder, offset + 1, printFunction.InputtData);
				}
				break;

				case CountFunctions countFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("COUNT"));
					PrintTreeRecursively(ref builder, offset + 1, countFunction.Url);
					PrintTreeRecursively(ref builder, offset + 1, countFunction.TagName);
				}
				break;

				case IntToStringFunctions i2sFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("INT_TO_STRING"));
					PrintTreeRecursively(ref builder, offset + 1, i2sFunction.Vallue);
				}
				break;

				case ToFileFunctions toFileFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("TO_FILE"));
					PrintTreeRecursively(ref builder, offset + 1, toFileFunction.Url);
				}
				break;

				case VisittFunctions visitFunction:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine(String.Format("VISIT"));
					PrintTreeRecursively(ref builder, offset + 1, visitFunction.Url);
				}
				break;

				default:
				{
					AppendSpaces(ref builder, offset);
					builder.AppendLine("UNKNOWN");
				}
				break;
			}
		}

		void AppendSpaces(ref StringBuilder builder, int count)
		{
			for (int i = 0; i < count; ++i)
			{
				builder.Append("    ");
			}
		}
	}
}
