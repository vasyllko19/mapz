﻿namespace Interpretator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.TextBox = new System.Windows.Forms.TextBox();
			this.MagicButton = new System.Windows.Forms.Button();
			this.Console = new System.Windows.Forms.RichTextBox();
			this.NotMagicButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TextBox
			// 
			this.TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.TextBox.BackColor = System.Drawing.SystemColors.MenuText;
			this.TextBox.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.TextBox.ForeColor = System.Drawing.Color.Lime;
			this.TextBox.Location = new System.Drawing.Point(12, 12);
			this.TextBox.Multiline = true;
			this.TextBox.Name = "TextBox";
			this.TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TextBox.Size = new System.Drawing.Size(461, 370);
			this.TextBox.TabIndex = 0;
			// 
			// MagicButton
			// 
			this.MagicButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.MagicButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.MagicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.MagicButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.MagicButton.Location = new System.Drawing.Point(198, 388);
			this.MagicButton.Name = "MagicButton";
			this.MagicButton.Size = new System.Drawing.Size(275, 45);
			this.MagicButton.TabIndex = 1;
			this.MagicButton.Text = "Execute";
			this.MagicButton.UseVisualStyleBackColor = false;
			this.MagicButton.Click += new System.EventHandler(this.MagicButton_Click);
			// 
			// Console
			// 
			this.Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Console.BackColor = System.Drawing.SystemColors.MenuText;
			this.Console.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Console.ForeColor = System.Drawing.SystemColors.Info;
			this.Console.Location = new System.Drawing.Point(479, 12);
			this.Console.Name = "Console";
			this.Console.Size = new System.Drawing.Size(441, 421);
			this.Console.TabIndex = 2;
			this.Console.Text = "";
			// 
			// NotMagicButton
			// 
			this.NotMagicButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.NotMagicButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.NotMagicButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.NotMagicButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.NotMagicButton.Location = new System.Drawing.Point(12, 388);
			this.NotMagicButton.Name = "NotMagicButton";
			this.NotMagicButton.Size = new System.Drawing.Size(180, 45);
			this.NotMagicButton.TabIndex = 3;
			this.NotMagicButton.Text = "Print tokens and tree";
			this.NotMagicButton.UseVisualStyleBackColor = false;
			this.NotMagicButton.Click += new System.EventHandler(this.NotMagicButton_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.ClientSize = new System.Drawing.Size(929, 440);
			this.Controls.Add(this.NotMagicButton);
			this.Controls.Add(this.Console);
			this.Controls.Add(this.MagicButton);
			this.Controls.Add(this.TextBox);
			this.Name = "MainWindow";
			this.Text = "Інтерпретатор";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.Button MagicButton;
        private System.Windows.Forms.RichTextBox Console;
		private System.Windows.Forms.Button NotMagicButton;
	}
}

