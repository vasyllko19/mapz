﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using xNet;
//------------------------------------------------------------------------------
namespace Interpretator
{

    class VisittFunctionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public VisittFunctionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if (tokens[position].getType() != TokenType.Visit)
                return null;
            //checking the syntax
            if (tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception($"Visit function syntax error: '(' is missing!");
            //getting the url
            position++;
            var url = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception($"Visit function syntax error: ')' is missing!");
            else
            {
                position++;
                return new VisittFunctions(url);
            }
        }
    }



    class VisittFunctions : IExpression
    {
        public IExpression Url { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            //trying to connect
            try
            {
                new HttpRequest().Get((string)Url.Execute(context));
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        //----------------------------------------------------------------------
        public VisittFunctions(IExpression url)
        {
            Url = url;
        }
    }
    //--------------------------------------------------------------------------
    
}
//------------------------------------------------------------------------------