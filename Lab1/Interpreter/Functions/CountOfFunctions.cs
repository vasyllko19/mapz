﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using AngleSharp.Parser.Html;
using xNet;
//------------------------------------------------------------------------------
namespace Interpretator
{
    //--------------------------------------------------------------------------
    //counts all the tags by name
    //--------------------------------------------------------------------------
    class CountFunctions : IExpression
    {
        public IExpression Url { get; private set; }
        public IExpression TagName { get; private set; }

		public object Execute(Contextt context)
        {
            //given url
            var url = (string) Url.Execute(context);
            //trying to connect
            try
            {
                new HttpRequest().Get(url);
            }
            catch (Exception)
            {
                throw new Exception($"Unable to connect with {url}, check URL-address and your Internet connection!");
            }

            var tagName = (string)TagName.Execute(context);

            var parser = new HtmlParser();
            var document = parser.Parse(new WebClient().DownloadString(url));
            //selecting all the tags by the name and return their amount
            return document.QuerySelectorAll(tagName).Length;
        }
        //----------------------------------------------------------------------
        public CountFunctions(IExpression url, IExpression tagName)
        {
            Url = url;
            TagName = tagName;
        }
    }
    //--------------------------------------------------------------------------
    class CountFunctionsCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token type
            if (tokens[position].getType() != TokenType.Count)
                return null;
            else
            {
                if (tokens[++position].getType() != TokenType.LeftParenthesis)
                    throw new Exception("Count function syntax error: '(' is missing!");

                position++;
                var url = _analyzer.GetCurrentExpressions(tokens, ref position);

                if (tokens[position].getType() != TokenType.Comma)
                    throw new Exception("Count function syntax error: ',' is missing!");

                position++;
                var tagName = _analyzer.GetCurrentExpressions(tokens, ref position);

                if (tokens[position++].getType() != TokenType.RightParenthesis)
                    throw new Exception("Count function syntax error: ')' is missing!");
                else
                    return new CountFunctions(url, tagName);
            }
        }
        //----------------------------------------------------------------------
        public CountFunctionsCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
       
    }
}
//------------------------------------------------------------------------------