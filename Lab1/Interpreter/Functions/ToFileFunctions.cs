﻿//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using xNet;
//------------------------------------------------------------------------------
namespace Interpretator
{
    class ToFileFunctions : IExpression
    {
        public ToFileFunctions(IExpression url)
        {
            Url = url;
        }

        public IExpression Url { get; private set; }
		//----------------------------------------------------------------------
		public object Execute(Contextt context)
        {
            var url = (string)Url.Execute(context);
            //trying to connect
            try
            {
                new HttpRequest().Get(url);
            }
            catch (Exception)
            {
                throw new Exception($"Unable to connect with {url}, check URL-address and your Internet connection!");
            }
            //write down html code
            var htmlString = new WebClient().DownloadString(url);
            //rewriting to the file
            try
            {
                System.IO.File.WriteAllText(@".\txts\html.txt", htmlString);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //----------------------------------------------------------------------
       
    }
    //--------------------------------------------------------------------------
    class ToFileCreatorr : IExpressionCreator
    {
        private SyntaxxAnalyzer _analyzer;
       
        //----------------------------------------------------------------------
        public IExpression CreateExpressions(ref List<Token> tokens, ref int position)
        {
            //checking token's type
            if(tokens[position].getType() != TokenType.HtmlToFile)
                return null;
            //checking the syntax
            if(tokens[++position].getType() != TokenType.LeftParenthesis)
                throw new Exception("ToFile function syntax error: '(' is missing!");

            position++;
            var url = _analyzer.GetCurrentExpressions(tokens, ref position);

            if (tokens[position].getType() != TokenType.RightParenthesis)
                throw new Exception("ToFile function syntax error: ')' is missing!");
            else
            {
                position++;
                return new ToFileFunctions(url);
            }
        }

        //----------------------------------------------------------------------
        public ToFileCreatorr(SyntaxxAnalyzer analyzer)
        {
            _analyzer = analyzer;
        }
    }
}
//------------------------------------------------------------------------------