﻿//------------------------------------------------------------------------------
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
//------------------------------------------------------------------------------
namespace Interpretator
{
    public enum TokenType
    {
        //KEYWORDS
        If,
        Else,
        While,
        Do,
        EndIf,
        EndWhile,
        Visit,
        NumberConst,
        StringConst,
        Print,
        Undefined,
        Int,
        String,
        VariableName,
        Bool,
        True,
        False,
        Tag,
        Count,
        HtmlToFile,
        ToString,

        //PUNCTUATION
        Semicolon,
        Comma,
        LeftParenthesis,
        LeftBracket,
        RightBracket,
        RightParenthesis,
        End,
        
        //OPERATORS
        Plus,
        Minus,
        Multiply,
        Divide,
        More,
        Less,
        Assign,
        Equal,
        NotEqual,
        EqualLess,
        EqualMore
    }
    //--------------------------------------------------------------------------
    //single token to create dictionary
    //--------------------------------------------------------------------------
    public class Token
    {
        private TokenType _type;
        private string _lexem;
        //----------------------------------------------------------------------
        public Token(TokenType tType, string tLexem)
        {
            _type = tType;
            _lexem = tLexem;
        }
        //----------------------------------------------------------------------
        public TokenType getType()
        {
            return _type;
        }
        //----------------------------------------------------------------------
        public string GetLexem()
        {
            return _lexem;
        }
        //----------------------------------------------------------------------
        public override string ToString()
        {
            return _type.ToString() + ": [" + _lexem + "]";
        }
    }
    //--------------------------------------------------------------------------
    //Lexical analisys for the give code
    //separates strings into tokens
    //--------------------------------------------------------------------------
    class LexicAnalyzerr
    {
        //----------------------------------------------------------------------
        //serves to define lexems
        //----------------------------------------------------------------------
        public Dictionary<TokenType, string> Expressions = new Dictionary<TokenType, string>()
        {
            {TokenType.Tag, @"tag"},
            {TokenType.Count, @"count"},
            {TokenType.HtmlToFile, @"toFile"},
            {TokenType.ToString, @"IntToString"},
            {TokenType.While, @"while"},
            {TokenType.Do, @"do"},
            {TokenType.Visit, @"visit"},
            {TokenType.Print, @"print"},
            {TokenType.EqualMore, @">="},
            {TokenType.EqualLess, @"<="},
            {TokenType.Equal, @"=="},
            {TokenType.NotEqual, @"!="},
            {TokenType.If, @"if"},
            {TokenType.Plus, @"\+"},
            {TokenType.Minus, @"-"},
            {TokenType.Multiply, @"\*"},
            {TokenType.Divide, @"/"},
            {TokenType.Assign, @"="},
            {TokenType.StringConst, @"\"".*"""},
            {TokenType.NumberConst, @"-?[0-9]+"},
            {TokenType.Comma, @","},
            {TokenType.End, @"end"},
            {TokenType.Else, @"else"},
            {TokenType.LeftParenthesis, @"\("},
            {TokenType.RightParenthesis, @"\)"},
            {TokenType.LeftBracket, @"\{"},
            {TokenType.RightBracket, @"\}"},
            {TokenType.Less, @"<"},
            {TokenType.More, @">"},
            {TokenType.Int, @"int"},
            {TokenType.Bool, @"bool"},
            {TokenType.String, @"str"},
            {TokenType.True, @"TRUE"},
            {TokenType.False, @"FALSE" },
            {TokenType.VariableName, @"[A-Za-z][A-Za-z0-9_]*"}
        };
        //----------------------------------------------------------------------
        //analizing the code
        //----------------------------------------------------------------------
        public List<Token> Analyze(string[] codeLines)
        {
            int currPosition;                           
            var resultTokenList = new List<Token>();

            int fiasco;     //there's no tokens in the line

            foreach (var codeline in codeLines)
            {
                var line = DeleteExtraSpaces(codeline);
                Debug.WriteLine(line + "#");
                var regex = new Regex(@"^\s*$");
                Debug.WriteLine(regex.Match(line).Success + " $" + line + "$");
                if (regex.Match(line).Success) continue;

                currPosition = 0;
                while (currPosition < line.Length)
                {
                    fiasco = 0;
                    foreach (var tokenExpression in Expressions)
                    {
                        //checking all the tokens in every line
                        var re = new Regex(tokenExpression.Value, RegexOptions.Singleline); 
                        
                        var endPos = line.IndexOf(' ', currPosition + 1);
                        if (endPos < 0) endPos = line.Length;

                        var stringStart = line.IndexOf('\"', currPosition);     //first entry of '\"'
                        var stringEnd = line.IndexOf('\"', stringStart + 1);    //last entry of '\"'
                        if (endPos > stringStart && endPos < stringEnd)
                            endPos = stringEnd + 1;

                        if (line[currPosition] == ' ' || line[currPosition] == '\r')
                        {
                            ++currPosition;
                            continue;
                        }

                        Match match = re.Match(line, currPosition, endPos - currPosition);
                        //checking if there is a token in the line
                        if (match.Success && match.Index == currPosition)
                        {
                            //determination of the string
                            if (tokenExpression.Key == TokenType.StringConst)
                            {
                                var lexem = match.Value.Substring(1, match.Length - 2);        
                                resultTokenList.Add(new Token(tokenExpression.Key, lexem));
                                currPosition = match.Index + match.Length;
                                break;
                            }
                            else
                            {
                                resultTokenList.Add(new Token(tokenExpression.Key, match.Value));
                                currPosition = match.Index + match.Length;
                                break;
                            }
                        }
                        else fiasco++;  //there's no tokens in the line
                        //tokens are not valid
                        if (fiasco >= Expressions.Count)
                        {
                            Debug.WriteLine("SyntaxError");
                            break;
                        }
                    }
                }
            }
            return resultTokenList;
        }
        //----------------------------------------------------------------------
        //getting rid of all the spaces
        //----------------------------------------------------------------------
        public static string DeleteExtraSpaces(string code)
        {
            var re = new Regex(@"([ ]{2,})");
            var res = re.Replace(code, @" ");
            return new Regex(@"([ ]{1,}$)").Replace(res, @"");
        }
    }
}
//------------------------------------------------------------------------------