﻿#include <iostream>
#include <map>
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>

#include <string>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;



class SomeComplexCommand : public MyCommand
{
private:
	ThisReceiver* somereceive;
	string First;
	string Second;

public:
	SomeComplexCommand(ThisReceiver* rec, std::string aa, std::string bb) : somereceive(rec), First(aa), Second(bb)
	{
	}
	void Executethis() const override
	{
		std::cout << "Графiчна команда : рендеринг нових об'єктiв\n";
		this->somereceive->DoSomework(this->First);
		this->somereceive->DoSomeElse(this->Second);
	}
};


class thisInvoker
{
private:
	MyCommand* startttt;

	MyCommand* finishhh;

public:
	~thisInvoker()
	{
		delete startttt;
		delete finishhh;
	}

	void StartSet(MyCommand* command)
	{
		this->startttt = command;
	}
	void FinishSet(MyCommand* command)
	{
		this->finishhh = command;
	}

	void DoSomeImportantthing()
	{
		cout << "Рибак: вхiд в лоббi\n";
		if (this->startttt)
		{
			this->startttt->Executethis();
		}
		std::cout << "Рибак: рухається по картi\n";

		if (this->finishhh)
		{
			this->finishhh->Executethis();
		}
	}
};

class MyCommand
{
public:
	virtual ~MyCommand()
	{
	}
	virtual void Executethis() const = 0;
};

class SomeSimpleCommand : public MyCommand
{
private:
	string load_paythis;

public:
	explicit SomeSimpleCommand(string pay_load) : load_paythis(pay_load)
	{
	}
	void Executethis() const override
	{
		cout << "Текстова команда: виконується (" << this->load_paythis << ")\n";
	}
};


class ThisReceiver
{
public:
	void DoSomework(const string& aaaa)
	{
		cout << "Розпiзнавач подiї : опрацьовує (" << aaaa << ".)\n";
	}
	void DoSomeElse(const std::string& bbbb)
	{
		cout << "Розпiзнавач подiї : опрацьовує (" << bbbb << ".)\n";
	}
};





int main()
{
	setlocale(LC_CTYPE, "ukr");
	thisInvoker* invoker = new thisInvoker;
	invoker->StartSet(new SomeSimpleCommand("Привiтати гравця"));
	ThisReceiver* receiver = new ThisReceiver;
	invoker->StartSet(new SomeSimpleCommand("Привiтати гравця"));
	ThisReceiver* receiver = new ThisReceiver;
	invoker->FinishSet(new SomeComplexCommand(receiver, "Рух вправо", "Рух влiво"));
	invoker->DoSomeImportantthing();

	delete invoker;
	delete receiver;

	return 0;
}
