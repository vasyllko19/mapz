﻿#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <typeinfo>
#include <iostream>
#include <list>
#include <string>
#include <mutex>
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>
#include <iostream>
#include <list>
#include <string>


using namespace std;


class ThisSubject : public MySubject
{
public:
	virtual ~ThisSubject()
	{
		cout << "Чат закрито.\n";
	}

	void thisAttachhh(MyObserver* observer) override
	{
		observer_of_list.push_back(observer);
	}
	void thisDetachhh(MyObserver* observer) override
	{
		observer_of_list.remove(observer);
	}
	void thisNotifyyy() override
	{
		list<MyObserver*>::iterator iterator = observer_of_list.begin();
		HowMuchObserver();
		while (iterator != observer_of_list.end())
		{
			(*iterator)->DataUpdate(message_);
			++iterator;
		}
	}

class MyObserver
{
public:
	virtual ~MyObserver() {};
	virtual void DataUpdate(const std::string& message_from_subject) = 0;
};


void CodeOfClient()
{
	ThisSubject* subject = new ThisSubject;
	ThisObserver* Firstobserver = new ThisObserver(*subject);
	ThisObserver* secondobserver = new ThisObserver(*subject);
	ThisObserver* thirdobserver = new ThisObserver(*subject);
	ThisObserver* fourthobserver;
	ThisObserver* fifthobserver;

	subject->CreateSomeNewMessage("Рибак Ivan1983 зловив окуня 1.2кг !");
	thirdobserver->RemoveFromList();

	subject->CreateSomeNewMessage("Рибак Ivan1983 продав окуня 1.2кг :D");
	fourthobserver = new ThisObserver(*subject);

	secondobserver->RemoveFromList();
	fifthobserver = new ThisObserver(*subject);

	subject->CreateSomeNewMessage("Рибак Ivan1983 покинув лоббi");
	fifthobserver->RemoveFromList();

	fourthobserver->RemoveFromList();
	Firstobserver->RemoveFromList();
	secondobserver->RemoveFromList();
	fifthobserver = new ThisObserver(*subject);

	subject->CreateSomeNewMessage("Рибак Ivan1983 покинув лоббi");
	fifthobserver->RemoveFromList();

	fourthobserver->RemoveFromList();
	Firstobserver->RemoveFromList();

	delete fifthobserver;
	delete fourthobserver;
	delete thirdobserver;
	delete secondobserver;
	delete Firstobserver;
	delete subject;
}

class MySubject
{
public:
	virtual ~MySubject() {};
	virtual void thisAttachhh(MyObserver* observer) = 0;
	virtual void thisDetachhh(MyObserver* observer) = 0;
	virtual void thisNotifyyy() = 0;
};



	void CreateSomeNewMessage(std::string message = "Empty")
	{
		this->message_ = message;
		thisNotifyyy();
	}
	void HowMuchObserver()
	{
		cout << "Спостерiгачiв у грi " << observer_of_list.size() << "\n\n";
	}
private:
	std::list<MyObserver*> observer_of_list;
	std::string message_;
};

class ThisObserver : public MyObserver
{
public:
	ThisObserver(ThisSubject& subject) : subjectttt_(subject)
	{
		this->subjectttt_.thisAttachhh(this);
		cout << "Привiт, я спостерiгач \"" << ++ThisObserver::number_static << "\".\n";
		this->number_ = ThisObserver::number_static;
	}
	virtual ~ThisObserver()
	{
		cout << "Бувай, я спостерiгач \"" << this->number_ << "\".\n";
	}

	void DataUpdate(const std::string& message_from_subject) override
	{
		subject_message = message_from_subject;
		PrintData();
	}
	void RemoveFromList()
	{
		subjectttt_.thisDetachhh(this);
		cout << "Спостерiгач \"" << number_ << "\" виключений зi списку. \n";
	}
	void PrintData()
	{
		cout << "Спостерiгач \"" << this->number_ << "\": нове повiдомлення у чатi : " << this->subject_message << "\n";
	}

private:
	string subject_message;
	ThisSubject& subjectttt_;
	static int number_static;
	int number_;
};

int ThisObserver::number_static = 0;



int main()
{
	setlocale(LC_CTYPE, "ukr");
	CodeOfClient();
	return 0;
}
